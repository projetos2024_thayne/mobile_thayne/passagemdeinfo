import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Supermercado Rilu",
    },
    {
      id: 2,
      title: "Ir para escola",
      date: "2024-09-10",
      time: "06:00",
      address: "Sesi",
    },
    {
      id: 3,
      title: "Ir ao Shopping",
      date: "2024-08-19",
      time: "10:00",
      address: "Shopping center",
    },
  ];
    
    const taskPress=(task)=>{
        navigation.navigate('DetalhesDasTarefas',{task})

    }
  return (
    <View>
      <FlatList 
      data={tasks}
      keyExtractor={(item)=> item.id.toString}
      renderItem={({item})=>(
        <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
        </TouchableOpacity>
      )}
      />
    </View>
  );
};
export default TaskList;
